FROM centos:7.5.1804

MAINTAINER Andrey Erokhin <andrey.erokhin@cern.ch>

ENV LANGUAGE=en_US.UTF-8 LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 PYTHONIOENCODING=UTF-8

RUN	\
	yum -y install \
	epel-release \
	https://centos7.iuscommunity.org/ius-release.rpm \
 && curl -sL https://rpm.nodesource.com/setup_10.x | bash - \
 && systemd-machine-id-setup \
 && rm -rf /var/cache/yum/

RUN	\
	yum -y install \
	authconfig \
	sssd-client \
	nano \
	mc \
	gcc-c++ \
	texlive-latex \
	texlive-texmf-fonts \
	texlive-adjustbox \
	texlive-collectbox \
	wget \
	which \
	python36u python36u-libs python36u-devel python36u-pip \
	nodejs \
	bzip2 \
	xz \
	unzip \
	patch \
 && authconfig --enablesssd --update \
 && rm -rf /var/cache/yum/

RUN yum -y install \
	motif-devel mesa-libGL-devel mesa-libGLU-devel \
	libXpm \
	tk \
	libpng12 \
&& rm -rf /var/cache/yum/

RUN	\
	wget https://xpra.org/repos/CentOS/xpra.repo -O /etc/yum.repos.d/xpra.repo \
 &&	yum -y install \
	git \
	jq \
	moreutils \
	xpra-2.4.2 python-websockify xterm \
 &&	rm -rf /var/cache/yum/

RUN pip3.6 install --no-cache-dir \
	'notebook==5.5.0' \
	'jupyterhub==0.9.0' \
	'jupyterlab==0.34.1' \
	'jupyter_nbextensions_configurator' \
 &&	(find / -depth \( \( -type d -a \( -name test -o -name tests \) \) -o \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \) -exec rm -rf '{}' + 2>/dev/null || true)

# Add ipykernel and its dependencies to an isolated place referenced by PYTHONPATH in user env (Py3)
# Needed to prevent updated Jupyter code to break with older LCG versions (this way it picks always the correct pkgs)
RUN mkdir -p /usr/local/lib/swan && pip3.6 install --no-cache-dir 'ipykernel==4.8.2' -t /usr/local/lib/swan \
 &&	(find / -depth \( \( -type d -a \( -name test -o -name tests \) \) -o \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \) -exec rm -rf '{}' + 2>/dev/null || true)
